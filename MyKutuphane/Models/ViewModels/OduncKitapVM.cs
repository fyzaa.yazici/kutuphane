﻿using MyKutuphane.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Models.ViewModels
{
    public class OduncKitapVM
    {
        public int KitapId { get; set; }

        public int UyeId { get; set; }

        public DateTime? AlısTarihi { get; set; }

        public DateTime? GetirecegiTarih { get; set; }

        public DateTime? GetirdigiTarih { get; set; }

        public byte? IslemDurum { get; set; }

        public Uye Uye { get; set; }
    }
}
