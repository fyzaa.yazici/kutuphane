﻿
using MyKutuphane.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Models.ViewModels
{
    public class KitapListVM
    {
        public Kitap kitap { get; set; }
        public int Id { get; set; }
        public string Ad { get; set; }

        public int SıraNo { get; set; }

        public int Adet { get; set; }

        public DateTime EklenmeTarihi { get; set; }

        public int YazarId { get; set; }

       public  Yazar Yazar { get; set; }

        public Kategori Kategori { get; set; }

    }
}
