﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Models.ViewModels
{
    public class YazarVM
    {
        public int Id { get; set; }
        public string Ad { get; set; }

        public string Soyad { get; set; }
    }
}
