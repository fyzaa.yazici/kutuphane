﻿
using MyKutuphane.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Models.ViewModels
{
    public class KategoriListVM
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Image { get; set; }

        public List<Kitap> Kitaplar { get; set; }
    }
}
