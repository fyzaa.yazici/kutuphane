﻿
using Microsoft.EntityFrameworkCore;
using MyKutuphane.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.DataAccess.Context
{
    public class KutuphaneDbContext: DbContext
    {
        public KutuphaneDbContext()
        {

        }

        public KutuphaneDbContext(DbContextOptions<KutuphaneDbContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseMySQL(Environment.GetEnvironmentVariable("MYSQL_URI"));
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Kategori> Kategoriler { get; set; }

        public DbSet<Kitap> Kitaplar { get; set; }

        public DbSet<OduncKitap> OduncKitaplar { get; set; }

        public DbSet<Uye> Uyeler { get; set; }

        public DbSet<Yazar> Yazarlar { get; set; }

    }
}
