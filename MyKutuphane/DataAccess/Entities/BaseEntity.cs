﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.DataAccess.Entities
{
    public interface BaseEntity
    {
        public int Id { get; set; }
    }
}
