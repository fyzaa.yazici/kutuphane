﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.DataAccess.Entities
{
    public class Kitap :BaseEntity
    {

        public int Id { get; set; }
      
        public string Ad { get; set; }

        public int SıraNo { get; set; }

        public int Adet { get; set; }

        public DateTime EklenmeTarihi { get; set; }

        public  Yazar Yazar { get; set; }

        public  Kategori Kategori { get; set; }




    }
}
