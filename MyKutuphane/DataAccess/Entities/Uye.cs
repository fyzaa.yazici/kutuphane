﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.DataAccess.Entities
{
    public class Uye
    {
        public int Id { get; set; } 
        public string Ad { get; set; }
        public string Soyad { get; set; }

        public string TCNO { get; set; }

        public string Telefon { get; set; }

        public DateTime KayitTarihi { get; set; }

        public string Mail { get; set; }

        public string Sifre { get; set; }

        public int? Ceza { get; set; }

        public int? Yetki { get; set; }

       // public List<OduncKitap> OduncKitaplar { get; set; }
    }
}
