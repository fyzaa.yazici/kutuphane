﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.DataAccess.Entities
{
    public class OduncKitap:BaseEntity
    {
        public int Id { get; set; }
        public int KitapId { get; set; }

        public int UyeId { get; set; }

        public DateTime? AlisTarihi { get; set; }

        public DateTime? GetirecegiTarih { get; set; }

        public DateTime? GetirdigiTarih { get; set; }

        public bool? IslemDurum { get; set; }

        public Uye Uye { get; set; }

        public Kitap Kitap { get; set; }
    }
}
