﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.DataAccess.Entities
{
    public class Yazar:BaseEntity
    {
        public int Id { get; set; }

        public string Ad { get; set; }
        public string Soyad { get; set; }
    }

}
