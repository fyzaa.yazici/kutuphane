﻿using Microsoft.AspNetCore.Mvc;
using MyKutuphane.DataAccess.Context;
using MyKutuphane.DataAccess.Entities;
using MyKutuphane.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Controllers
{
    public class KategoriController : Controller
    {
        private readonly KutuphaneDbContext _context;



        public KategoriController(KutuphaneDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {

            var kategoriList = _context.Kategoriler.Select(q =>
             new Kategori
             {
                 Ad=q.Ad,
                 Image=q.Image
                
             }
            ).ToList();
            return View(kategoriList);
        }
    }
}
