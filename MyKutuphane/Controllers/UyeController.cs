﻿
using Microsoft.AspNetCore.Mvc;
using MyKutuphane.DataAccess.Context;
using MyKutuphane.DataAccess.Entities;
using MyKutuphane.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Controllers
{
    public class UyeController : Controller
    {
        private readonly KutuphaneDbContext _context;
        public UyeController(KutuphaneDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var uyelist = _context.Uyeler.Select(q => new Uye
            {
                Id = q.Id,
                Ad = q.Ad,
                Soyad = q.Soyad,
                TCNO = q.TCNO,
                Telefon = q.Telefon,
                Mail = q.Mail
            }).ToList();
            return View(uyelist);
        }

        public IActionResult UyeEkle()
        {
           // UyeListVM kitap = new UyeListVM();
            return View();
        }
        [HttpPost]
        public IActionResult UyeEkle(Uye uyeekle)
        {
            var yeniuye = new Uye
            {
                Ad = uyeekle.Ad,
                Soyad = uyeekle.Soyad,
                TCNO = uyeekle.TCNO,
                Telefon = uyeekle.Telefon,
                KayitTarihi = uyeekle.KayitTarihi,
              

            };
            _context.Add(yeniuye);
            var result = _context.SaveChanges();

            return RedirectToAction("Index");

        }

        public IActionResult UyeSil(int id)
        {
            var uyeSil = _context.Uyeler.FirstOrDefault(q => q.Id == id);
            _context.Uyeler.Remove(uyeSil);
            var result = _context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult UyeDuzenle(int id1)
        {
            var uyeDuzenle = _context.Uyeler.FirstOrDefault(q => q.Id == id1);
            return View(uyeDuzenle);
        }

        [HttpPost]
        public IActionResult UyeDuzenle(Uye uyeDuzenle,int id)
        {
            var yeniUye = _context.Uyeler.FirstOrDefault(q => q.Id == id);
            yeniUye.Ad = uyeDuzenle.Ad;
            yeniUye.Soyad = uyeDuzenle.Soyad;
            yeniUye.TCNO = uyeDuzenle.TCNO;
            yeniUye.Telefon = uyeDuzenle.Telefon;
            yeniUye.KayitTarihi = uyeDuzenle.KayitTarihi;
            _context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
