﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyKutuphane.DataAccess.Context;
using MyKutuphane.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Controllers
{
    public class OduncKitapIslemController : Controller
    {
        private readonly KutuphaneDbContext _context;
        public OduncKitapIslemController(KutuphaneDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult OduncKitapVer()
        {

           
            var uyelist = _context.Uyeler.ToList();
             ViewBag.uyelistem = uyelist;

            var kitapList = _context.Kitaplar.ToList();

            ViewBag.kitaplistem = kitapList;



            return View();


        }

        [HttpPost]
        public IActionResult OduncKitapVer(OduncKitap odunc)
        {
            //    var userID = odunc.UyeId;

          //  var uyeOdunc = _context.Uyeler.FirstOrDefault(q=>q.Id==odunc.UyeId);
            var yeniOdunc = new OduncKitap
            {

                UyeId = odunc.UyeId,
                KitapId = odunc.KitapId,
                AlisTarihi=odunc.AlisTarihi,
                GetirecegiTarih=odunc.GetirecegiTarih,
                GetirdigiTarih=odunc.GetirdigiTarih,
              
                
            };
            var gelenkitap = _context.Kitaplar.Select(q => q.Id).Count();

            var kitapkontrol = _context.Kitaplar.Select(q => q.Adet<=gelenkitap);

            if (kitapkontrol !=null)
            {

            }
            _context.OduncKitaplar.Add(yeniOdunc);
            var result = _context.SaveChanges();


            return RedirectToAction("Index","Kitap");


        }

        public IActionResult OduncKitapAl()
        {
            return View();
        }

        public IActionResult OduncKitapListele()
        {

            return View();
        }
    }
}
