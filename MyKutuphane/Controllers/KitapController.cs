﻿
using Microsoft.AspNetCore.Mvc;
using MyKutuphane.DataAccess.Context;
using MyKutuphane.DataAccess.Entities;
using MyKutuphane.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyKutuphane.Controllers
{
    public class KitapController : Controller
    {
        private readonly KutuphaneDbContext _context;
        public KitapController(KutuphaneDbContext context)
        {
            _context = context;
        }

      
        public IActionResult Index()
        {
            var kitaplist = _context.Kitaplar.Select(q=> new Kitap
            {
                Id = q.Id,
                Ad =q.Ad,
                Yazar=q.Yazar,
               Kategori=q.Kategori,
                SıraNo=q.SıraNo,
                Adet=q.Adet,
                EklenmeTarihi=q.EklenmeTarihi
            }).ToList();
          
            return View(kitaplist);
        }

        public IActionResult KitapEkle()
        {
            Kitap kitap = new Kitap();
            return View();
        }
        [HttpPost]
        public IActionResult KitapEkle(Kitap kitapekle)
        {
            var yenikitap = new Kitap
            {
               
                Ad = kitapekle.Ad,
                Adet= kitapekle.Adet,
                SıraNo= kitapekle.SıraNo,
                Yazar= kitapekle.Yazar,
                EklenmeTarihi = kitapekle.EklenmeTarihi,
                Kategori = kitapekle.Kategori

            };
            _context.Add(yenikitap);
            var result = _context.SaveChanges();

            return RedirectToAction("Index");

        }

        public IActionResult KitapSil(int id)
        {
            var kitap = _context.Kitaplar.FirstOrDefault(q => q.Id == id);
            _context.Kitaplar.Remove(kitap);
            var result = _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
